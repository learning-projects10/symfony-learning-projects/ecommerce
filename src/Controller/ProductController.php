<?php


namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController {

    //============================ CREATE ============================

    /**
     * @Route ("/products/create", name="createProduct", methods={"GET","POST"})
     */
    public function createProduct(Request $request){
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();
            return $this->redirectToRoute('products');
        }

        return $this->render('products/createProduct.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    //============================ SEARCH ============================

    /**
     * @Route ("/products", name="products", methods="GET")
     */
    public function products(){
        $productsRepository = $this->getDoctrine()->getRepository(Product::class);

        return $this->render('products/products.html.twig', [
            'products' => $productsRepository->findByQuantityNotNull(),
        ]);
    }

    /**
     * @Route ("/products/search", name="searchProduct", methods="GET")
     */
    public function searchProduct(Request $request){
        $productsRepository = $this->getDoctrine()->getRepository(Product::class);

        return $this->render('products/searchProduct.html.twig', [
            'products' => $productsRepository->findByTitle($request->query->get('search')),
        ]);
    }

    /**
     * @Route("/products/{id}", name="singleProduct")
     */
    public function singleProduct(Product $product) {
        return $this->render('products/singleProduct.html.twig', [
            'product' => $product,
        ]);
    }

    //============================ EDIT ============================

    /**
     * @Route("/products/{id}/edit", name="editProduct",  methods={"GET","POST"})
     */
    public function editProduct(Request $request, Product $product) {

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('products');
        }

        return $this->render('products/editProduct.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    //============================ DELETE ============================

    /**
     * @Route("/products/{id}/delete", name="deleteProduct",  methods="DELETE")
     */
    public function deleteProduct(Product $product) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();

        return $this->redirectToRoute('products');
    }
}