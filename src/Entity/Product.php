<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 50,
     *     maxMessage = "Le titre ne peut pas dépasser {{ limit }} caractères."
     * )
     * @Assert\NotBlank(
     *     message="Le titre ne peut pas être vide."
     * )
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 500,
     *     maxMessage = "La description ne peut pas dépasser {{ limit }} caractères."
     * )
     * @Assert\NotBlank(
     *     message="Le titre ne peut pas être vide."
     * )
     * @ORM\Column(type="string", length=500)
     */
    private $description;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur doit être un nombre."
     * )
     * @Assert\Positive(
     *     message="La valeur doit être positive."
     * )
     * @Assert\NotBlank(
     *     message="Le prix ne peut pas être vide."
     * )
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur doit être un nombre."
     * )
     * @Assert\Positive(
     *     message="La valeur doit être positive."
     * )
     * @Assert\NotBlank(
     *     message="La quantité ne peut pas être vide."
     * )
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Assert\Url(
     *    message = "L'url n'est pas une url valide",
     * )
     * @ORM\Column(type="string", length=2083, nullable=true)
     */
    private $linkPhoto;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     */
    private $category;

    public function __construct() {
        $this->setCreatedAt( new DateTime() );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getLinkPhoto(): ?string
    {
        return $this->linkPhoto;
    }

    public function setLinkPhoto(string $linkPhoto): self
    {
        $this->linkPhoto = $linkPhoto;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
